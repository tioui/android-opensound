package com.tioui.opensound;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.IOException;

/**
 * Main activity of the audio player
 *
 * @author Louis Marchand (prog@tioui.com)
 * @version 0.1, Sat, 23 Oct 2021 14:37:04 +0000
 *
 * MIT license.
 */

public class MainActivity extends AppCompatActivity implements Runnable,
        SeekBar.OnSeekBarChangeListener {

    /**
     * The interval between each {@link #seekBar} update.
     */
    private final static int TIMER_INTERVAL = 500;

    /**
     * The internal player used to play audio files
     */
    MediaPlayer player;

    /**
     * A timer used to update the visual player
     */
    Handler timerHandler;

    /**
     * The visual progress bar of the interface
     */
    SeekBar seekBar;

    /**
     * Launched when the Activity is launched.
     *
     * @param aSavedInstanceState The state of the saved instance if any
     */
    @Override
    protected void onCreate(Bundle aSavedInstanceState) {
        super.onCreate(aSavedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        timerHandler = new Handler();
        seekBar = findViewById(R.id.progress_bar);
        if (seekBar != null) {
            seekBar.setOnSeekBarChangeListener(this);
        }
        initialisePlayer();
    }

    /**
     * Used to instanciate and prepare {@link #player`}.
     */
    private void initialisePlayer(){
        Intent lIntent = getIntent();
        Uri lUri = lIntent.getData();

        player = new MediaPlayer();
        try {
            player.setDataSource(getApplicationContext(), lUri);
            player.prepare();
            player.start();
            timerHandler.postDelayed(this, TIMER_INTERVAL);
        } catch (IOException e) {
            Toast lToast = Toast.makeText(getApplicationContext(), "Cannot open sound.", Toast.LENGTH_SHORT);
            lToast.show();
        }
    }

    /**
     * Launched by the timer at regular interval.
     *
     * @see #TIMER_INTERVAL
     */
    @Override
    public void run() {
        if (player != null) {
            int lDuration = player.getDuration();
            int lTimestamp = (int) (player.getTimestamp().getAnchorMediaTimeUs() / 1000);
            if (seekBar != null && lDuration != 0) {
                seekBar.setProgress((lTimestamp * seekBar.getMax()) / lDuration);
            }
            timerHandler.postDelayed(this, TIMER_INTERVAL);
        }
    }

    /**
     * Launched when the Close button is clicked.
     *
     * @param aView The button that has been clicked (unused)
     */
    public void onCloseClick(View aView){
        finish();
    }

    /**
     * Launched when the Restart (rewind) button is clicked.
     *
     * @param aView The button that has been clicked (unused)
     */
    public void onRestartClick(View aView){
        player.seekTo(0);
        if (!player.isPlaying()){
            player.start();
        }
    }

    /**
     * Launched when the Pause button is clicked.
     *
     * @param aView The button that has been clicked (unused)
     */
    public void onPauseClick(View aView) {
        if (player.isPlaying()) {
            player.pause();
        } else {
            player.start();
        }
    }

    /**
     * Launched when the activity is stopped.
     */
    @Override
    protected void onStop() {
        super.onStop();
        player.stop();
        player.reset();
        player.release();
        player = null;
    }

    /**
     * Launched when the user change the {@link #seekBar} value (unused).
     *
     * @param aSeekBar The bar that had it's value changed.
     * @param aProgress The new value.
     * @param aFromUser True if the new value was set by the user.
     */
    @Override
    public void onProgressChanged(SeekBar aSeekBar, int aProgress, boolean aFromUser) {

    }

    /**
     * Launched when the user start using the {@link #seekBar} (unused)
     *
     * @param aSeekBar The SeekBar that is used by the user
     */
    @Override
    public void onStartTrackingTouch(SeekBar aSeekBar) {

    }

    /**
     * Launched when the user stop using the {@link #seekBar}
     *
     * @param aSeekBar The SeekBar that is used by the user
     */
    @Override
    public void onStopTrackingTouch(SeekBar aSeekBar) {
        player.seekTo((player.getDuration() * aSeekBar.getProgress()) / aSeekBar.getMax());
        if (!player.isPlaying()){
            player.start();
        }
    }
}