<p>This is a simple Intent that can be used to easily open any sound file, from any app.</p>

<p>When an application has to open an audio sound file (like when a file browser need to open an MP3 file or when an Email client needs to open a WAV file in attachment of a received Email), you have the option to open it with this app.</p>

<p>Contrary to other audio players, this player does not use Android media library. So it does not fail to open when the file is not in the Android media library.</p>

<p>In other words, it is just: Open the file, play it and close (with a KISS philosophy).</p>
