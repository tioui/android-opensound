Change in 0.1
-------------

* Creating core functionnality

Change in 0.2
-------------

* Adding ChangeLog

Change in 0.3
-------------

* Adding Fastlane

Change in 0.4
-------------

* Bug fix when cannot get media duration.

Change in 0.5
-------------

* Typo in the faslane description.
